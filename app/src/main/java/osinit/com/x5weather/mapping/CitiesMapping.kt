package osinit.com.x5weather.mapping

import osinit.com.x5weather.db.models.CityEntity
import osinit.com.x5weather.models.City

object CitiesMapping {

    fun cityEntitiesToCities(cities: List<CityEntity>): List<City> {
        return cities.map {
            City(id = it.id, name = it.name, county = it.country, latitude = it.latitude, longitude = it.longitude, saved = it.saved)
        }
    }

    fun cityEntityToCity(cityEntity: CityEntity): City =
        City(id = cityEntity.id, name = cityEntity.name, county = cityEntity.country, latitude = cityEntity.latitude, longitude = cityEntity.longitude, saved = cityEntity.saved)

}