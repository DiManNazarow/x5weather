package osinit.com.x5weather.mapping

import osinit.com.x5weather.db.models.CurrentWeatherEntity
import osinit.com.x5weather.db.models.DailyWeatherEntity
import osinit.com.x5weather.db.models.HourlyWeatherEntity
import osinit.com.x5weather.db.models.WeatherEntity
import osinit.com.x5weather.logic.WeatherLogic
import osinit.com.x5weather.models.CurrentWeather
import osinit.com.x5weather.models.DayWeather
import osinit.com.x5weather.models.HourWeather
import osinit.com.x5weather.models.Weather
import osinit.com.x5weather.networking.models.FullForecastResponseData
import osinit.com.x5weather.utils.toPopString
import osinit.com.x5weather.utils.toTemperatureString
import osinit.com.x5weather.utils.toWindSpeedString
import java.util.Locale

object WeatherMapping {

    /**
     * Converts response model to DB model
     */
    fun fullForecastResponseToCurrentWeatherEntity(cityId: Long, responseData: FullForecastResponseData): CurrentWeatherEntity {
        return CurrentWeatherEntity(
            id = cityId,
            currentTemp = responseData.current.temp,
            feelsLike = responseData.current.feelsLike,
            currentCondition = responseData.current.weather.firstOrNull()?.description,
            weatherIcon = responseData.current.weather.firstOrNull()?.icon
        )
    }

    /**
     * Converts response model to DB models
     */
    fun fullForecastResponseToHourlyWeatherEntities(cityId: Long, responseData: FullForecastResponseData): List<HourlyWeatherEntity> {
        return responseData.hourly.map { data ->
            HourlyWeatherEntity(
                id = 0,
                cityId = cityId,
                time = data.dt,
                temp = data.temp,
                feelsLike = data.feelsLike,
                humidity = data.humidity,
                currentCondition = data.weather.firstOrNull()?.description,
                weatherIcon = data.weather.firstOrNull()?.icon,
                pop = data.pop,
                windDegree = data.windDeg,
                windSpeed = data.windSpeed
            )
        }
    }

    /**
     * Converts response model to DB models
     */
    fun fullForecastResponseToDailyWeatherEntities(cityId: Long, responseData: FullForecastResponseData): List<DailyWeatherEntity> {
        return responseData.daily.map { data ->
            DailyWeatherEntity(
                id = 0,
                cityId = cityId,
                time = data.dt,
                dayTemp = data.temp.day,
                dayTempFeelsLike = data.feelsLike.day,
                nightTemp = data.temp.night,
                nightTempFeelsLike = data.feelsLike.night,
                humidity = data.humidity,
                weatherIcon =  data.weather.firstOrNull()?.icon,
                windDegree = data.windDeg,
                windSpeed = data.windSpeed,
                pop = data.pop
            )
        }
    }

    /**
     * Converts DB model to domain model
     */
    fun weatherEntityToWeather(entity: WeatherEntity): Weather {
        return Weather(
            currentWeather = currentWeatherEntityToCurrentWeather(entity.currentWeather),
            dailyWeather = entity.dailyWeather.map {
                DayWeather(
                    time = WeatherLogic.longTimeToDayWithDate(it.time * 1000L),
                    dayTemp = it.dayTemp.toTemperatureString(),
                    dayTempFeelsLike = it.dayTempFeelsLike.toTemperatureString(),
                    nightTemp = it.nightTemp.toTemperatureString(),
                    nightTempFeelsLike = it.nightTempFeelsLike.toTemperatureString(),
                    humidity = it.humidity.toString(),
                    iconLink = WeatherLogic.createWeatherIconUrl(it.weatherIcon),
                    windDegree = it.windDegree.toString(),
                    windSpeed = it.windSpeed.toWindSpeedString(),
                    pop = (it.pop * 100).toPopString()
                )
            },
            hourlyWeather = entity.hourlyWeather.map {
                HourWeather(
                    time = it.time * 1000L,
                    temp = it.temp.toTemperatureString(),
                    feelsLike = it.feelsLike.toTemperatureString(),
                    humidity = it.humidity.toString(),
                    description = it.currentCondition ?: "",
                    iconLink = WeatherLogic.createWeatherIconUrl(it.weatherIcon),
                    windSpeed = it.windSpeed.toWindSpeedString(),
                    windDegree = it.windDegree.toString(),
                    pop = (it.pop * 100).toPopString()
                )
            }
        )
    }

    /**
     * Converts db's entity to domain model
     */
    private fun currentWeatherEntityToCurrentWeather(currentWeatherEntity: CurrentWeatherEntity): CurrentWeather {
        return CurrentWeather(
            currentTemp = currentWeatherEntity.currentTemp.toTemperatureString(),
            currentCondition = currentWeatherEntity.currentCondition?.capitalize(Locale.getDefault()) ?: "",
            feelsLike = currentWeatherEntity.feelsLike.toTemperatureString(),
            weatherIcon = WeatherLogic.createWeatherIconUrl(currentWeatherEntity.weatherIcon)
        )
    }

}