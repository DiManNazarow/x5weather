package osinit.com.x5weather.ui.dailyforecast

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import osinit.com.x5weather.models.Weather
import osinit.com.x5weather.ui.adapters.DetailDailyViewHolderItemData
import osinit.com.x5weather.ui.adapters.ListItem

class DailyForecastFragmentViewModel : ViewModel() {

    private var dailyWeatherItems: List<ListItem<DetailDailyViewHolderItemData>> = emptyList()
        set(value) {
            field = value
            _dailyWeather.value = dailyWeatherItems
        }

    private val _dailyWeather = MutableLiveData<List<ListItem<DetailDailyViewHolderItemData>>>()
    val dailyWeather: LiveData<List<ListItem<DetailDailyViewHolderItemData>>> = _dailyWeather

    fun onWeatherLoaded(weather: Weather) {
        fillWeatherItems(weather)
    }

    private fun fillWeatherItems(weather: Weather) {
        dailyWeatherItems = weather.dailyWeather.map {
            ListItem(DetailDailyViewHolderItemData(
                time = it.time,
                dayTemp = it.dayTemp,
                nightTemp = it.nightTemp,
                humidity = it.humidity,
                iconUrl = it.iconLink,
                windDegree = it.windDegree,
                windSpeed = it.windSpeed,
                pop = it.pop
            ))
        }
    }

}