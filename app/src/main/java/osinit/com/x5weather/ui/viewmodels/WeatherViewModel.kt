package osinit.com.x5weather.ui.viewmodels

import androidx.annotation.StringRes
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import osinit.com.x5weather.R
import osinit.com.x5weather.logic.WeatherLogic
import osinit.com.x5weather.models.CurrentWeather
import osinit.com.x5weather.models.DayWeather
import osinit.com.x5weather.models.HourWeather
import osinit.com.x5weather.models.WEATHER_STUB
import osinit.com.x5weather.models.Weather
import osinit.com.x5weather.networking.models.NetworkResponse
import osinit.com.x5weather.ui.adapters.DailyViewHolderItemData
import osinit.com.x5weather.ui.adapters.HourlyViewHolderItemData
import osinit.com.x5weather.ui.adapters.ListItem
import osinit.com.x5weather.usecases.city.GetCityUseCase
import osinit.com.x5weather.usecases.city.GetSavedCityIdUseCase
import osinit.com.x5weather.usecases.weather.GetWeatherUseCase
import osinit.com.x5weather.usecases.weather.RequestWeatherUseCase
import osinit.com.x5weather.usecases.weather.SaveWeatherUseCase

/**
 * View model to control weather loading
 */
class WeatherViewModel(
    private val requestCurrentWeatherUseCase: RequestWeatherUseCase,
    private val saveCurrentWeatherUseCase: SaveWeatherUseCase,
    private val getCurrentWeatherUseCase: GetWeatherUseCase,
    private val getCityUseCase: GetCityUseCase,
    private val getSavedCityIdUseCase: GetSavedCityIdUseCase
) : ViewModel() {

    private val _showRefresh = MutableLiveData<Boolean>()
    val showRefresh: LiveData<Boolean> = _showRefresh

    private val _errorMessage = MutableLiveData<Int>()
    val errorMessage: LiveData<Int> = _errorMessage

    private var _weather: Weather? = null
        set(value) {
            field = value
            _weatherLD.value = value
            fillWeather(value)
        }
    private val _weatherLD = MutableLiveData<Weather>()
    val weatherLD: LiveData<Weather> = _weatherLD

    val currentWeather = ObservableField<CurrentWeather>()

    private var _forecastByHours: List<ListItem<HourlyViewHolderItemData>> = emptyList()
        set(value) {
            field = value
            forecastByHours.set(value)
        }
    val forecastByHours = ObservableField<List<ListItem<HourlyViewHolderItemData>>>()

    private var _forecastByDays: List<ListItem<DailyViewHolderItemData>> = emptyList()
        set(value) {
            field = value
            forecastByDays.set(value)
        }
    val forecastByDays = ObservableField<List<ListItem<DailyViewHolderItemData>>>()

    fun onDashboardViewCreated() {
        fillFromStub()
    }

    fun onDailyForecastViewCreated() {
        loadWeatherFromDB()
    }

    fun onHourlyForecastViewCreated() {
        loadWeatherFromDB()
    }

    fun onCityChanged() {
        updateWeather()
    }

    fun onRefreshCalled() {
        updateWeather()
    }

    /**
     * Updated weather for API
     */
    private fun updateWeather() {
        _showRefresh.value = true
        viewModelScope.launch {
            val city = getCityUseCase.getCityById(getSavedCityIdUseCase.getSavedCityId()) ?: return@launch
            val response = requestCurrentWeatherUseCase.request(city.latitude, city.longitude)
            when (response) {
                is NetworkResponse.Success -> {
                    response.body ?: throw IllegalStateException("Empty body is not allowed")
                    // update weather in db after each request
                    saveCurrentWeatherUseCase.save(response.body)
                    getWeatherFromDB()
                    _showRefresh.value = false
                }
                is NetworkResponse.ApiError -> {
                    loadFromDBAndDisableRefresh(R.string.error_data_loading_failed)
                }
                is NetworkResponse.NetworkError -> { // No internet case is also here
                    loadFromDBAndDisableRefresh(R.string.error_data_loading_failed)
                }
                is NetworkResponse.UnknownError -> {
                    loadFromDBAndDisableRefresh(R.string.error_data_loading_failed)
                }
            }
        }
    }

    private fun fillFromStub() {
        _showRefresh.value = true
        _weather = WEATHER_STUB
    }

    /**
     * Public method to load weather from DB
     */
    private fun loadWeatherFromDB() {
        viewModelScope.launch {
            getWeatherFromDB()
        }
    }

    private suspend fun loadFromDBAndDisableRefresh(@StringRes errorMessage: Int?) {
        getWeatherFromDB(errorMessage = errorMessage)
        _showRefresh.value = false
    }

    /**
     * Internal method to get weather from DB
     */
    private suspend fun getWeatherFromDB(@StringRes errorMessage: Int? = null) {
        val weather: Weather? = getCurrentWeatherUseCase.getWeather()
        if (weather != null) {
           _weather = weather
        } else {
            _weather = WEATHER_STUB
        }
        if (errorMessage != null) {
            _errorMessage.value = errorMessage
        }
    }

    private fun fillForecastByHours(byHours: List<HourWeather>) {
        _forecastByHours = byHours.map { hourData ->
            ListItem(
                HourlyViewHolderItemData(
                    temp = hourData.temp,
                    time = WeatherLogic.longTimeToHourAndMinAndDay(hourData.time),
                    pop = "${hourData.pop} %",
                    iconUrl = hourData.iconLink
                )
            )
        }
    }

    private fun fillForecastByDays(byDays: List<DayWeather>) {
        _forecastByDays = byDays.map { dayData ->
            ListItem(
                DailyViewHolderItemData(
                    time = dayData.time,
                    nightTemp = dayData.nightTemp,
                    dayTemp = dayData.dayTemp,
                    pop = "${dayData.pop} %",
                    iconUrl = dayData.iconLink
                )
            )
        }
    }

    private fun fillWeather(weather: Weather?) {
        if (weather != null) {
            currentWeather.set(weather.currentWeather)
            fillForecastByHours(weather.hourlyWeather)
            fillForecastByDays(weather.dailyWeather)
        }
    }

}