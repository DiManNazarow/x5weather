package osinit.com.x5weather.ui.viewmodels

import android.text.Editable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import osinit.com.x5weather.models.City
import osinit.com.x5weather.ui.adapters.CityViewHolderItemData
import osinit.com.x5weather.ui.adapters.ListItem
import osinit.com.x5weather.usecases.city.SearchCitiesUseCase
import osinit.com.x5weather.usecases.city.GetCityUseCase
import osinit.com.x5weather.usecases.city.GetSavedCitiesUseCase
import osinit.com.x5weather.usecases.city.GetSavedCityIdUseCase
import osinit.com.x5weather.usecases.city.SaveCityIdUseCase
import osinit.com.x5weather.usecases.city.SaveCityUseCase
import osinit.com.x5weather.utils.MOSCOW_CITY_ID

/**
 * View model to control cities
 */
class CitiesViewModel(
    private val searchCitiesUseCase: SearchCitiesUseCase,
    private val getCityUseCase: GetCityUseCase,
    private val getSavedCitiesUseCase: GetSavedCitiesUseCase,
    private val getSavedCityIdUseCase: GetSavedCityIdUseCase,
    private val saveCityIdUseCase: SaveCityIdUseCase,
    private val saveCityUseCase: SaveCityUseCase
) : ViewModel() {

    private var _cityItems: List<ListItem<CityViewHolderItemData>> = emptyList()
        set(value) {
            field = value
            _cities.value = value
        }
    private val _cities = MutableLiveData<List<ListItem<CityViewHolderItemData>>>()
    val cities: LiveData<List<ListItem<CityViewHolderItemData>>> = _cities

    private val _citiesVisibility = MutableLiveData<Boolean>()
    val citiesVisibility: LiveData<Boolean> = _citiesVisibility

    private val _searchViewVisibility = MutableLiveData<Boolean>()
    val searchViewVisibility: LiveData<Boolean> = _searchViewVisibility

    private val _searchIconVisibility = MutableLiveData<Boolean>()
    val searchIconVisibility: LiveData<Boolean> = _searchIconVisibility

    private val _darkLayerVisibility = MutableLiveData<Boolean>()
    val darkLayerVisibility: LiveData<Boolean> = _darkLayerVisibility

    private val _city = MutableLiveData<City>()
    val city: LiveData<City> = _city

    private var searchClause: String? = null

    var isSearchActivated: Boolean = false

    fun onViewCreated() {
        getSavedCity()
    }

    fun onCitySelected(cityId: Long) {
        viewModelScope.launch {
            saveCityIdUseCase.saveCityId(cityId)
            saveCityUseCase.save(cityId)
            getSavedCity()
            clearSearch()
        }
    }

    fun onSearchTextChanged(text: Editable?) {
        if (text != null && text.isNotEmpty()) {
            val search = text.toString().trim()
            searchClause = search
            viewModelScope.launch {
                delay(200)
                if (search != searchClause) return@launch
                showCitiesList(searchCitiesUseCase.search(searchClause))
            }
        } else {
            searchClause = null
            showSavedCities()
        }
    }

    fun onSearchCityClicked() {
        _citiesVisibility.value = true
        _searchViewVisibility.value = true
        _searchIconVisibility.value = false
        _darkLayerVisibility.value = true
        isSearchActivated = true
        showSavedCities()
    }

    fun onCloseSearchClicked() {
        _citiesVisibility.value = false
        _searchViewVisibility.value = false
        _searchIconVisibility.value = true
        _darkLayerVisibility.value = false
        _citiesVisibility.value = false
        isSearchActivated = false
    }

    private fun getSavedCity() {
        viewModelScope.launch {
            val savedCity = getCityUseCase.getCityById(getSavedCityIdUseCase.getSavedCityId())
            if (savedCity != null) {
                _city.value = savedCity
                _city.value = null // nullified value to disable action when value populated automatically
            } else {
                // if no selected city selects MOSCOW by default
                saveCityIdUseCase.saveCityId(MOSCOW_CITY_ID)
                _city.value = getCityUseCase.getCityById(getSavedCityIdUseCase.getSavedCityId())
            }
        }
    }

    private fun showCitiesList(cities: List<City>) {
        _cityItems = cities.map { city ->
            ListItem(CityViewHolderItemData(id = city.id, name = city.name, country = city.county))
        }
    }

    private fun showSavedCities() {
        viewModelScope.launch {
            showCitiesList(getSavedCitiesUseCase.get())
        }
    }

    private fun clearSearch() {
        searchClause = null
        onCloseSearchClicked()
        showSavedCities()
    }

}