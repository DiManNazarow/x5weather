package osinit.com.x5weather.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import osinit.com.x5weather.databinding.LayoutDailyViewHolderBinding

/**
 * Adapter for horizontal daily forecast (dashboard fragment)
 */
class DailyForecastRecyclerAdapter : AbsRecyclerAdapter<DailyViewHolder, DailyViewHolderItemData>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DailyViewHolder =
        DailyViewHolder(parent)

    override fun onBindViewHolder(holder: DailyViewHolder, position: Int) {
        holder.setData(items[position].data)
    }

    override fun onViewRecycled(holder: DailyViewHolder) {
        holder.onViewRecycled()
    }

}

class DailyViewHolder(parent: ViewGroup) : AbsViewHolder<DailyViewHolderItemData, LayoutDailyViewHolderBinding>(
    LayoutDailyViewHolderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
) {

    override fun fill() {
        binding.dayTempTextView.text = itemData.dayTemp
        binding.nightTempTextView.text = itemData.nightTemp
        binding.timeTextView.text = itemData.time
        binding.popTextView.text = itemData.pop
        itemData.iconUrl?.let {
            Glide.with(itemView).load(it).into(binding.weatherIcon)
        }
    }

    override fun onViewRecycled() {
        Glide.with(itemView).clear(binding.weatherIcon)
    }

}

data class DailyViewHolderItemData(
    val time: String,
    val dayTemp: String,
    val nightTemp: String,
    val pop: String,
    val iconUrl: String?
) : ListItemData()