package osinit.com.x5weather.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide

import osinit.com.x5weather.databinding.LayoutHourlyViewHolderBinding

/**
 * Adapter for horizontal hourly forecast (dashboard fragment)
 */
class HourlyForecastRecyclerAdapter : AbsRecyclerAdapter<HourlyViewHolder, HourlyViewHolderItemData>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HourlyViewHolder =
        HourlyViewHolder(parent)

    override fun onBindViewHolder(holder: HourlyViewHolder, position: Int) {
        holder.setData(items[position].data)
    }

    override fun onViewRecycled(holder: HourlyViewHolder) {
        holder.onViewRecycled()
    }

}

class HourlyViewHolder(parent: ViewGroup) : AbsViewHolder<HourlyViewHolderItemData, LayoutHourlyViewHolderBinding>(
    LayoutHourlyViewHolderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
) {

    override fun fill() {
        binding.tempTextView.text = itemData.temp
        binding.timeTextView.text = itemData.time
        binding.popTextView.text = itemData.pop
        itemData.iconUrl?.let {
            Glide.with(itemView).load(it).into(binding.weatherIcon)
        }
    }

    override fun onViewRecycled() {
        Glide.with(itemView).clear(binding.weatherIcon)
    }

}

data class HourlyViewHolderItemData(
    val time: String,
    val temp: String,
    val pop: String,
    val iconUrl: String?
) : ListItemData()

