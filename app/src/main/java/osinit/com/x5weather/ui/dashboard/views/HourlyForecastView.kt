package osinit.com.x5weather.ui.dashboard.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import osinit.com.x5weather.R
import osinit.com.x5weather.databinding.LayoutHourlyForecastBinding
import osinit.com.x5weather.ui.adapters.HourlyForecastRecyclerAdapter
import osinit.com.x5weather.ui.adapters.HourlyViewHolderItemData
import osinit.com.x5weather.ui.adapters.ListItem

class HourlyForecastView : ConstraintLayout {

    private lateinit var binding: LayoutHourlyForecastBinding

    private lateinit var adapter: HourlyForecastRecyclerAdapter

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(context)
    }

    private fun init(context: Context) {
        binding = LayoutHourlyForecastBinding.inflate(LayoutInflater.from(context), this, true)
        adapter = HourlyForecastRecyclerAdapter()
        binding.viewPager.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        binding.viewPager.adapter = adapter
        binding.viewPager.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.HORIZONTAL).apply {
            setDrawable(ResourcesCompat.getDrawable(context.resources, R.drawable.hourly_weather_recycler_divider, null)!!)
        })
    }

    fun setForecast(forecast: List<ListItem<HourlyViewHolderItemData>>) {
        adapter.items = forecast
    }

    fun setProgressState(show: Boolean) {
        if (show) {
            binding.shimmer.showShimmer(true)
        } else {
            binding.shimmer.hideShimmer()
        }
    }

}