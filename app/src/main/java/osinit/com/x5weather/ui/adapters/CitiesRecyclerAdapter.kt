package osinit.com.x5weather.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import osinit.com.x5weather.databinding.LayoutCityViewHolderBinding

class CitiesRecyclerAdapter : AbsRecyclerAdapter<CityViewHolder, CityViewHolderItemData>() {

    var onItemClicked: ((Long) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityViewHolder =
        CityViewHolder(parent)

    override fun onBindViewHolder(holder: CityViewHolder, position: Int) {
        holder.setData(items[position].data)
        holder.onItemClicked = onItemClicked
    }

}

class CityViewHolder(parent: ViewGroup) : AbsViewHolder<CityViewHolderItemData, LayoutCityViewHolderBinding>(
    LayoutCityViewHolderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
) {

    var onItemClicked: ((Long) -> Unit)? = null

    init {
        binding.root.setOnClickListener {
            onItemClicked?.invoke(itemData.id)
        }
    }

    override fun fill() {
        binding.cityNameTextView.text = itemData.name
        binding.countryTextView.text = itemData.country
    }

}

data class CityViewHolderItemData(
    val id: Long,
    val name: String,
    val country: String
) : ListItemData()