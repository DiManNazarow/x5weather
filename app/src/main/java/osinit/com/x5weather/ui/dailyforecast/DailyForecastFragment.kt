package osinit.com.x5weather.ui.dailyforecast

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import osinit.com.x5weather.databinding.FragmentDailyForecastBinding
import osinit.com.x5weather.ui.viewmodels.CitiesViewModel
import osinit.com.x5weather.ui.adapters.DetailDailyForecastRecyclerAdapter
import osinit.com.x5weather.ui.viewmodels.WeatherViewModel

class DailyForecastFragment : Fragment() {

    private val citiesViewModel: CitiesViewModel by sharedViewModel()

    private val weatherViewModel: WeatherViewModel by viewModel()

    private val fragmentViewModel: DailyForecastFragmentViewModel by viewModel()

    private lateinit var binding: FragmentDailyForecastBinding

    private lateinit var adapter: DetailDailyForecastRecyclerAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDailyForecastBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        citiesViewModel.city.observe(viewLifecycleOwner, { city ->
            if (city != null) {
                weatherViewModel.onCityChanged()
            }
        })
        weatherViewModel.weatherLD.observe(viewLifecycleOwner, fragmentViewModel::onWeatherLoaded)
        weatherViewModel.errorMessage.observe(viewLifecycleOwner, { message ->
            Snackbar.make(binding.root, message, Snackbar.LENGTH_LONG).show()
        })
        weatherViewModel.showRefresh.observe(viewLifecycleOwner, { show ->
            binding.swipeRefresh.isRefreshing = show
        })
        fragmentViewModel.dailyWeather.observe(viewLifecycleOwner, { items ->
            adapter.items = items
        })

        adapter = DetailDailyForecastRecyclerAdapter()
        binding.forecastRecycler.adapter = adapter

        binding.swipeRefresh.setOnRefreshListener(weatherViewModel::onRefreshCalled)

        weatherViewModel.onDailyForecastViewCreated()
    }

}