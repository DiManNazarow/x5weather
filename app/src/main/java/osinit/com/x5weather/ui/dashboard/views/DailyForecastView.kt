package osinit.com.x5weather.ui.dashboard.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import osinit.com.x5weather.R
import osinit.com.x5weather.databinding.LayoutDailyForecastBinding
import osinit.com.x5weather.ui.adapters.DailyForecastRecyclerAdapter
import osinit.com.x5weather.ui.adapters.DailyViewHolderItemData
import osinit.com.x5weather.ui.adapters.ListItem

class DailyForecastView : ConstraintLayout {

    private lateinit var binding: LayoutDailyForecastBinding

    private lateinit var adapter: DailyForecastRecyclerAdapter

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
    context,
    attrs,
    defStyleAttr
    ) {
        init(context)
    }

    private fun init(context: Context) {
        binding = LayoutDailyForecastBinding.inflate(LayoutInflater.from(context), this, true)
        adapter = DailyForecastRecyclerAdapter()
        binding.dailyRecycler.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        binding.dailyRecycler.adapter = adapter
        binding.dailyRecycler.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.HORIZONTAL).apply {
            setDrawable(ResourcesCompat.getDrawable(context.resources, R.drawable.hourly_weather_recycler_divider, null)!!)
        })
    }

    fun setForecast(forecast: List<ListItem<DailyViewHolderItemData>>) {
        adapter.items = forecast
    }

    fun setProgressState(show: Boolean) {
        if (show) {
            binding.shimmer.showShimmer(true)
        } else {
            binding.shimmer.hideShimmer()
        }
    }

}