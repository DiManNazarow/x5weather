package osinit.com.x5weather.ui.adapters

import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding

abstract class AbsRecyclerAdapter<VH : RecyclerView.ViewHolder, T : ListItemData> : RecyclerView.Adapter<VH>() {

    var items: List<ListItem<T>> = ArrayList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int = items[position].viewHolderType

}

abstract class AbsViewHolder<T : ListItemData, Binding : ViewBinding>(
    protected val binding: Binding
) : RecyclerView.ViewHolder(binding.root) {

    protected lateinit var itemData: T

    protected abstract fun fill()

    fun setData(itemData: T) {
        this.itemData = itemData
        fill()
    }

    open fun onViewRecycled() {}

}

data class ListItem<T : ListItemData>(val data: T, val viewHolderType: Int = 0)

open class ListItemData