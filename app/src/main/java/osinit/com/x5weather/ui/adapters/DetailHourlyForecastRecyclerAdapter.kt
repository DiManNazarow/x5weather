package osinit.com.x5weather.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import osinit.com.x5weather.R
import osinit.com.x5weather.databinding.LayoutHourWeatherDetailViewHolderBinding

/**
 * Adapter for full hourly forecast (hourlyforecast fragment)
 */
class DetailHourlyForecastRecyclerAdapter : AbsRecyclerAdapter<DetailHourlyViewHolder, DetailHourlyViewHolderItemData>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailHourlyViewHolder =
        DetailHourlyViewHolder(parent)

    override fun onBindViewHolder(holder: DetailHourlyViewHolder, position: Int) {
        holder.setData(items[position].data)
    }

    override fun onViewRecycled(holder: DetailHourlyViewHolder) {
        holder.onViewRecycled()
    }

}

class DetailHourlyViewHolder(parent: ViewGroup) : AbsViewHolder<DetailHourlyViewHolderItemData, LayoutHourWeatherDetailViewHolderBinding>(
    LayoutHourWeatherDetailViewHolderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
) {

    override fun fill() {
        binding.timeTextView.text = itemData.time
        binding.tempTextView.text = itemData.temp
        binding.popTextView.text = itemView.context.getString(R.string.pop, itemData.pop)
        binding.windTextView.text = itemView.context.getString(R.string.wind, itemData.windSpeed, itemData.windDegree)
        itemData.iconUrl?.let {
            Glide.with(itemView).load(it).into(binding.weatherIcon)
        }
    }

    override fun onViewRecycled() {
        Glide.with(itemView).clear(binding.weatherIcon)
    }

}

data class DetailHourlyViewHolderItemData(
    val time: String,
    val temp: String,
    val pop: String,
    val windDegree: String,
    val windSpeed: String,
    val iconUrl: String?
) : ListItemData()