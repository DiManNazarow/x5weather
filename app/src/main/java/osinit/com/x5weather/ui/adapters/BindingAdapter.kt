package osinit.com.x5weather.ui.adapters

import androidx.databinding.BindingAdapter
import osinit.com.x5weather.models.CurrentWeather
import osinit.com.x5weather.ui.dashboard.views.CurrentConditionView
import osinit.com.x5weather.ui.dashboard.views.DailyForecastView
import osinit.com.x5weather.ui.dashboard.views.HourlyForecastView

@BindingAdapter("currentWeather")
fun setCurrentWeather(view: CurrentConditionView, currentWeather: CurrentWeather?) {
    if (currentWeather != null) {
        view.setWeather(currentWeather)
    }
}

@BindingAdapter("hourlyForecast")
fun setHourlyForecast(view: HourlyForecastView, forecast: List<ListItem<HourlyViewHolderItemData>>?) {
    if (forecast != null) {
        view.setForecast(forecast)
    }
}

@BindingAdapter("dailyForecast")
fun setDailyForecast(view: DailyForecastView, forecast: List<ListItem<DailyViewHolderItemData>>?) {
    if (forecast != null) {
        view.setForecast(forecast)
    }
}