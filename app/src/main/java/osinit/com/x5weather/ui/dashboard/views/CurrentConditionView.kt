package osinit.com.x5weather.ui.dashboard.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.bumptech.glide.Glide
import osinit.com.x5weather.R
import osinit.com.x5weather.databinding.LayoutCurrentConditionViewBinding
import osinit.com.x5weather.models.CurrentWeather

class CurrentConditionView : ConstraintLayout {

    private lateinit var binding: LayoutCurrentConditionViewBinding

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(context)
    }

    private fun init(context: Context) {
        binding = LayoutCurrentConditionViewBinding.inflate(LayoutInflater.from(context), this, true)
    }

    fun setWeather(weather: CurrentWeather) {
        binding.temperatureTextView.text = weather.currentTemp
        binding.conditionTextView.text = weather.currentCondition
        binding.feelsLikeTextView.text = context.getString(R.string.feels_like, weather.feelsLike)
        weather.weatherIcon?.let {
            Glide.with(this).load(it).into(binding.weatherIcon)
        }
    }

    fun setProgressState(show: Boolean) {
        if (show) {
            binding.shimmer.showShimmer(true)
        } else {
            binding.shimmer.hideShimmer()
        }
    }

}