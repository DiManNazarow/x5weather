package osinit.com.x5weather.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import osinit.com.x5weather.databinding.FragmentDashboardBinding
import osinit.com.x5weather.ui.viewmodels.CitiesViewModel
import osinit.com.x5weather.ui.viewmodels.WeatherViewModel

class DashboardFragment : Fragment() {

    private lateinit var binding: FragmentDashboardBinding

    private val weatherViewModel: WeatherViewModel by viewModel()

    private val citiesViewModel: CitiesViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDashboardBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewModel = weatherViewModel
        binding.lifecycleOwner = viewLifecycleOwner

        weatherViewModel.showRefresh.observe(viewLifecycleOwner, {
            binding.conditionView.setProgressState(it)
            binding.hourlyView.setProgressState(it)
            binding.dailyView.setProgressState(it)
        })
        weatherViewModel.errorMessage.observe(viewLifecycleOwner, { message ->
            Snackbar.make(binding.root, message, Snackbar.LENGTH_LONG).show()
        })
        citiesViewModel.city.observe(viewLifecycleOwner, { city ->
            if (city != null) {
                weatherViewModel.onCityChanged()
            }
        })

        binding.swipeRefresh.setOnRefreshListener {
            weatherViewModel.onRefreshCalled()
            binding.swipeRefresh.isRefreshing = false
        }

        citiesViewModel.onViewCreated()
        weatherViewModel.onDashboardViewCreated()
    }

}