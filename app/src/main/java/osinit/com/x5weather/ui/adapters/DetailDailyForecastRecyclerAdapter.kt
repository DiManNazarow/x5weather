package osinit.com.x5weather.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import osinit.com.x5weather.R
import osinit.com.x5weather.databinding.LayoutDayWeatherDetailViewHolderBinding

/**
 * Adapter for full daily forecast (dailyforecast fragment)
 */
class DetailDailyForecastRecyclerAdapter : AbsRecyclerAdapter<DetailDailyForecastViewHolder, DetailDailyViewHolderItemData>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DetailDailyForecastViewHolder = DetailDailyForecastViewHolder(parent)

    override fun onBindViewHolder(holder: DetailDailyForecastViewHolder, position: Int) {
        holder.setData(items[position].data)
    }

    override fun onViewRecycled(holder: DetailDailyForecastViewHolder) {
        holder.onViewRecycled()
    }

}

class DetailDailyForecastViewHolder(parent: ViewGroup) : AbsViewHolder<DetailDailyViewHolderItemData, LayoutDayWeatherDetailViewHolderBinding>(
    LayoutDayWeatherDetailViewHolderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
) {

    override fun fill() {
        binding.dateTextView.text = itemData.time
        binding.dayTempTextView.text = itemData.dayTemp
        binding.nightTempTextView.text = itemData.nightTemp
        binding.maxTempTextView.text = itemView.context.getString(R.string.feels_like, itemData.dayTemp)
        binding.minTempTextView.text = itemView.context.getString(R.string.feels_like, itemData.nightTemp)
        binding.windTextView.text = itemView.context.getString(R.string.wind, itemData.windSpeed, itemData.windDegree)
        binding.popTextView.text = itemView.context.getString(R.string.pop, itemData.pop)
        itemData.iconUrl?.let {
            Glide.with(itemView).load(it).into(binding.dayWeatherIcon)
        }
    }

    override fun onViewRecycled() {
        Glide.with(itemView).clear(binding.dayWeatherIcon)
    }

}

data class DetailDailyViewHolderItemData(
    val time: String,
    val dayTemp: String,
    val nightTemp: String,
    val humidity: String,
    val iconUrl: String?,
    val windDegree: String,
    val windSpeed: String,
    val pop: String
) : ListItemData()