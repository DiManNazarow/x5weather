package osinit.com.x5weather.ui.hourlyforecast

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import osinit.com.x5weather.logic.WeatherLogic
import osinit.com.x5weather.models.Weather
import osinit.com.x5weather.ui.adapters.DetailHourlyViewHolderItemData
import osinit.com.x5weather.ui.adapters.ListItem

class HourlyForecastFragmentViewModel : ViewModel() {

    private var hourlyWeatherItems: List<ListItem<DetailHourlyViewHolderItemData>> = emptyList()
        set(value) {
            field = value
            _hourlyWeather.value = hourlyWeatherItems
        }

    private val _hourlyWeather = MutableLiveData<List<ListItem<DetailHourlyViewHolderItemData>>>()
    val hourlyWeather: LiveData<List<ListItem<DetailHourlyViewHolderItemData>>> = _hourlyWeather

    fun onWeatherLoaded(weather: Weather) {
        fillWeatherItems(weather)
    }

    private fun fillWeatherItems(weather: Weather) {
        hourlyWeatherItems = weather.hourlyWeather.map {
            ListItem(
                DetailHourlyViewHolderItemData(
                    time = WeatherLogic.longTimeToHourAndMinAndDay(it.time),
                    temp = it.temp,
                    pop = it.pop,
                    iconUrl = it.iconLink,
                    windDegree = it.windDegree,
                    windSpeed = it.windSpeed
                )
            )
        }
    }

}