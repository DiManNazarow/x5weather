package osinit.com.x5weather.ui.hourlyforecast

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import com.google.android.material.snackbar.Snackbar
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import osinit.com.x5weather.databinding.FragmentHourlyForecastBinding
import osinit.com.x5weather.ui.viewmodels.CitiesViewModel
import osinit.com.x5weather.ui.adapters.DetailHourlyForecastRecyclerAdapter
import osinit.com.x5weather.ui.viewmodels.WeatherViewModel

class HourlyForecastFragment : Fragment() {

    private val citiesViewModel: CitiesViewModel by sharedViewModel()

    private val weatherViewModel: WeatherViewModel by viewModel()

    private val fragmentViewModel: HourlyForecastFragmentViewModel by viewModel()

    private lateinit var binding: FragmentHourlyForecastBinding

    private lateinit var adapter: DetailHourlyForecastRecyclerAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHourlyForecastBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        citiesViewModel.city.observe(viewLifecycleOwner, { city ->
            if (city != null) {
                weatherViewModel.onCityChanged()
            }
        })
        weatherViewModel.weatherLD.observe(viewLifecycleOwner, fragmentViewModel::onWeatherLoaded)
        weatherViewModel.errorMessage.observe(viewLifecycleOwner, { message ->
            Snackbar.make(binding.root, message, Snackbar.LENGTH_LONG).show()
        })
        weatherViewModel.showRefresh.observe(viewLifecycleOwner, { show ->
            binding.swipeRefresh.isRefreshing = show
        })
        fragmentViewModel.hourlyWeather.observe(viewLifecycleOwner, { items ->
            adapter.items = items
        })

        adapter = DetailHourlyForecastRecyclerAdapter()
        binding.hourlyRecycler.adapter = adapter
        binding.hourlyRecycler.addItemDecoration(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))

        binding.swipeRefresh.setOnRefreshListener(weatherViewModel::onRefreshCalled)

        weatherViewModel.onHourlyForecastViewCreated()
    }

}