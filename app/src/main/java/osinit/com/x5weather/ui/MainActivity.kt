package osinit.com.x5weather.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.DividerItemDecoration
import org.koin.androidx.viewmodel.ext.android.viewModel
import osinit.com.x5weather.R
import osinit.com.x5weather.databinding.ActivityMainBinding
import osinit.com.x5weather.ui.adapters.CitiesRecyclerAdapter
import osinit.com.x5weather.ui.viewmodels.CitiesViewModel
import osinit.com.x5weather.utils.hideKeyboard
import osinit.com.x5weather.utils.showKeyboard

class MainActivity : AppCompatActivity() {

    private val citiesViewModel: CitiesViewModel by viewModel()

    private lateinit var binding: ActivityMainBinding

    private lateinit var adapter: CitiesRecyclerAdapter

    private var menuItem: MenuItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        val navController = findNavController(R.id.nav_host_fragment)
        NavigationUI.setupWithNavController(binding.navView, navController)

        citiesViewModel.cities.observe(this, { items ->
            adapter.items = items
        })
        citiesViewModel.citiesVisibility.observe(this, { isVisible ->
            binding.citiesRecycler.isVisible = isVisible
        })
        citiesViewModel.searchViewVisibility.observe(this, { isVisible ->
            binding.cityInputLayout.isVisible = isVisible
            if (isVisible) {
                requestFocusAndOpenKeyboard()
            }
        })
        citiesViewModel.searchIconVisibility.observe(this, { isVisible ->
            menuItem?.isVisible = isVisible
        })
        citiesViewModel.darkLayerVisibility.observe(this, { isVisible ->
            binding.darkLayer.isVisible = isVisible
        })
        citiesViewModel.city.observe(this, { city ->
            val _city = city ?: return@observe
            binding.toolbar.title = _city.name
        })

        adapter = CitiesRecyclerAdapter().apply {
            onItemClicked = {
                citiesViewModel.onCitySelected(it)
                binding.cityEditText.text = null
                binding.cityEditText.hideKeyboard()
            }
        }
        binding.citiesRecycler.adapter = adapter
        binding.citiesRecycler.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        binding.cityInputLayout.setEndIconOnClickListener {
            citiesViewModel.onCloseSearchClicked()
            binding.cityEditText.text = null
            binding.cityEditText.hideKeyboard()
        }
        binding.cityEditText.addTextChangedListener {
            citiesViewModel.onSearchTextChanged(it)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.actions_menu, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        this.menuItem = menu.findItem(R.id.search_city)
        this.menuItem?.isVisible = !citiesViewModel.isSearchActivated
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.search_city -> {
                citiesViewModel.onSearchCityClicked()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun requestFocusAndOpenKeyboard() {
        binding.cityEditText.requestFocus()
        binding.cityEditText.showKeyboard()
    }

}