package osinit.com.x5weather.networking.core

import okhttp3.Request
import okhttp3.ResponseBody
import okio.Timeout
import osinit.com.x5weather.networking.models.NetworkResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Converter
import retrofit2.Response
import java.io.IOException

/**
 * Custom [Call] in order to make Retrofit return [NetworkResponse] when API calls is triggered
 * @param delegate - original [Call]
 * @param errorConverter - converter [Converter] for response body [ResponseBody]
 */
internal class NetworkResponseCall<S : Any, E : Any>(
    private val delegate: Call<S>,
    private val errorConverter: Converter<ResponseBody, E>
) : Call<NetworkResponse<S, E>> {

    override fun enqueue(callback: Callback<NetworkResponse<S, E>>) {
        return delegate.enqueue(object : Callback<S> {
            override fun onResponse(call: Call<S>, response: Response<S>) {
                val body = response.body()
                val code = response.code()
                val error = response.errorBody()
                if (response.isSuccessful) {
                    if (body != null) {
                        callback.onResponse(this@NetworkResponseCall, Response.success(
                            NetworkResponse.Success(body)
                        ))
                    } else {
                        // Response is successful but the body is null
                        callback.onResponse(this@NetworkResponseCall, Response.success(
                            NetworkResponse.Success(null)
                        ))
                    }
                } else {
                    val errorBody = when {
                        error == null -> null
                        error.contentLength() == 0L -> null
                        else -> try {
                            errorConverter.convert(error)
                        } catch (e: Exception) {
                            e.printStackTrace()
                            null
                        }
                    }
                    if (errorBody != null) {
                        // Response is successful but API returned error, there is error body
                        callback.onResponse(this@NetworkResponseCall, Response.success(
                            NetworkResponse.ApiError(errorBody, code)
                        ))
                    } else {
                        // Response is successful but API returned error, there is no error body
                        callback.onResponse(this@NetworkResponseCall, Response.success(
                            NetworkResponse.UnknownError(null)
                        ))
                    }
                }
            }

            override fun onFailure(call: Call<S>, throwable: Throwable) {
                val networkResponse = when (throwable) {
                    // There was an error while performing request
                    is IOException -> NetworkResponse.NetworkError(throwable)
                    else -> NetworkResponse.UnknownError(throwable)
                }
                callback.onResponse(this@NetworkResponseCall, Response.success(networkResponse))
            }
        })
    }

    override fun isExecuted(): Boolean = delegate.isExecuted

    override fun clone(): Call<NetworkResponse<S, E>> =
        NetworkResponseCall(
            delegate.clone(),
            errorConverter
        )

    override fun isCanceled(): Boolean = delegate.isCanceled

    override fun cancel() = delegate.cancel()

    override fun execute(): Response<NetworkResponse<S, E>> {
        throw UnsupportedOperationException("NetworkResponseCall doesn't support execute")
    }

    override fun request(): Request = delegate.request()

    override fun timeout(): Timeout = delegate.timeout()

}