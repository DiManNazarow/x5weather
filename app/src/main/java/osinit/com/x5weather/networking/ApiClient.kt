package osinit.com.x5weather.networking

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import osinit.com.x5weather.BuildConfig
import osinit.com.x5weather.networking.core.NetworkResponseAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiClient {

    companion object {

        fun create(): Api {
            val clientBuilder = OkHttpClient.Builder()
            if (BuildConfig.DEBUG) {
                val bodyLogInterceptor = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }
                val headerLogInterceptor = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.HEADERS }
                val basicLogInterceptor = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BASIC }
                clientBuilder.addInterceptor(bodyLogInterceptor).addInterceptor(headerLogInterceptor).addInterceptor(basicLogInterceptor)
            }
            return Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL)
                .addCallAdapterFactory(NetworkResponseAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(clientBuilder.build())
                .build()
                .create(Api::class.java)
        }

    }

}