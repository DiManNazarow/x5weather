package osinit.com.x5weather.networking.core

import okhttp3.ResponseBody
import osinit.com.x5weather.networking.models.NetworkResponse
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Converter
import java.lang.reflect.Type

/**
 * Adapts regular retrofit's call [Call] to lib's [NetworkResponseCall]
 */
internal class NetworkResponseAdapter<S : Any, E : Any>(
    private val successType: Type,
    private val errorBodyConverter: Converter<ResponseBody, E>
) : CallAdapter<S, Call<NetworkResponse<S, E>>> {

    override fun responseType(): Type = successType

    override fun adapt(call: Call<S>): Call<NetworkResponse<S, E>> {
        return NetworkResponseCall(
            call,
            errorBodyConverter
        )
    }

}