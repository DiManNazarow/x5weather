package osinit.com.x5weather.networking.models

import java.io.IOException

/**
 * Represent network response
 * @param Body - body of success response (nullable)
 * @param ErrorBody - body of API error response (nullable)
 */
sealed class NetworkResponse<out Body : Any, out ErrorBody : Any> {
    /**
     * Success response with body
     * @param body - body of response (nullable)
     */
    data class Success<Body : Any>(val body: Body?) : NetworkResponse<Body, Nothing>()
    /**
     * Failure response with body and code
     * @param body - body of response (nullable)
     * @param code - API error code
     */
    data class ApiError<ErrorBody : Any>(val body: ErrorBody?, val code: Int) : NetworkResponse<Nothing, ErrorBody>()
    /**
     * Network error
     * @param error - thrown exception
     */
    data class NetworkError(val error: IOException) : NetworkResponse<Nothing, Nothing>()
    /**
     * For example, json parsing error
     * @param error - thrown exception
     */
    data class UnknownError(val error: Throwable?) : NetworkResponse<Nothing, Nothing>()

}