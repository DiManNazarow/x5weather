package osinit.com.x5weather.networking.models

data class ErrorResponseData(
    val cod: Int,
    val message: String
)