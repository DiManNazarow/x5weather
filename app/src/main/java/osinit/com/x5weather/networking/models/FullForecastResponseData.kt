package osinit.com.x5weather.networking.models

import com.google.gson.annotations.SerializedName
import osinit.com.x5weather.utils.Latitude
import osinit.com.x5weather.utils.Longitude

data class FullForecastResponseData(
    @SerializedName("lat")
    val latitude: Latitude,
    @SerializedName("lon")
    val longitude: Longitude,
    @SerializedName("timezone")
    val timezone: String,
    @SerializedName("timezone_offset")
    val timezoneOffset: Int,
    @SerializedName("current")
    val current: Current,
    @SerializedName("minutely")
    val minutely: List<Minutely>,
    @SerializedName("hourly")
    val hourly: List<Hourly>,
    @SerializedName("daily")
    val daily: List<Daily>,
    @SerializedName("alerts")
    val alerts: List<Alerts>
) {

    data class Current(
        @SerializedName("dt")
        val dt: Long,
        @SerializedName("sunrise")
        val sunrise: Long,
        @SerializedName("sunset")
        val sunset: Long,
        @SerializedName("temp")
        val temp: Double,
        @SerializedName("feels_like")
        val feelsLike: Double,
        @SerializedName("pressure")
        val pressure: Int,
        @SerializedName("humidity")
        val humidity: Int,
        @SerializedName("dew_point")
        val dewPoint: Double,
        @SerializedName("uvi")
        val uvi: Double,
        @SerializedName("clouds")
        val clouds: Int,
        @SerializedName("visibility")
        val visibility: Int,
        @SerializedName("wind_speed")
        val windSpeed: Double,
        @SerializedName("wind_deg")
        val windDeg: Int,
        @SerializedName("weather")
        val weather: List<Weather>,
        @SerializedName("rain")
        val rain: Rain
    )

    data class Minutely(
        @SerializedName("dt")
        val dt: Long,
        @SerializedName("precipitation")
        val precipitation: Double
    )

    data class Hourly(
        @SerializedName("dt")
        val dt: Long,
        @SerializedName("temp")
        val temp: Double,
        @SerializedName("feels_like")
        val feelsLike: Double,
        @SerializedName("pressure")
        val pressure: Int,
        @SerializedName("humidity")
        val humidity: Int,
        @SerializedName("dew_point")
        val dewPoint: Double,
        @SerializedName("clouds")
        val clouds: Int,
        @SerializedName("visibility")
        val visibility: Int,
        @SerializedName("wind_speed")
        val windSpeed: Double,
        @SerializedName("wind_deg")
        val windDeg: Int,
        @SerializedName("weather")
        val weather: List<Weather>,
        @SerializedName("pop")
        val pop: Double,
        @SerializedName("rain")
        val rain: Rain
    )

    data class Daily(
        @SerializedName("dt")
        val dt: Long,
        @SerializedName("sunrise")
        val sunrise: Long,
        @SerializedName("sunset")
        val sunset: Long,
        @SerializedName("temp")
        val temp: Temp,
        @SerializedName("feels_like")
        val feelsLike: FeelsLike,
        @SerializedName("pressure")
        val pressure: Int,
        @SerializedName("humidity")
        val humidity: Int,
        @SerializedName("dew_point")
        val dewPoint: Double,
        @SerializedName("wind_speed")
        val windSpeed: Double,
        @SerializedName("wind_deg")
        val windDeg: Int,
        @SerializedName("weather")
        val weather: List<Weather>,
        @SerializedName("clouds")
        val clouds: Int,
        @SerializedName("pop")
        val pop: Double,
        @SerializedName("rain")
        val rain: Double,
        @SerializedName("uvi")
        val uvi: Double
    ) {

        data class Temp(
            @SerializedName("day")
            val day: Double,
            @SerializedName("min")
            val min: Double,
            @SerializedName("max")
            val max: Double,
            @SerializedName("night")
            val night: Double,
            @SerializedName("eve")
            val eve: Double,
            @SerializedName("morn")
            val morn: Double
        )

        data class FeelsLike(
            @SerializedName("day")
            val day: Double,
            @SerializedName("night")
            val night: Double,
            @SerializedName("eve")
            val eve: Double,
            @SerializedName("morn")
            val morn: Double
        )

    }

    data class Alerts(
        @SerializedName("sender_name")
        val senderName: String,
        @SerializedName("event")
        val event: String,
        @SerializedName("start")
        val start: Long,
        @SerializedName("end")
        val end: Long,
        @SerializedName("description")
        val description: String
    )

}