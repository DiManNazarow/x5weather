package osinit.com.x5weather.networking

import osinit.com.x5weather.networking.models.ErrorResponseData
import osinit.com.x5weather.networking.models.FullForecastResponseData
import osinit.com.x5weather.networking.models.NetworkResponse
import osinit.com.x5weather.utils.Latitude
import osinit.com.x5weather.utils.Longitude
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    /**
     * Requests full weather forecast
     * @param latitude - location latitude
     * @param longitude - location longitude
     * @param lang - language of response https://openweathermap.org/api/one-call-api#multi
     * @param units - type of measurement units
     * @param appId - API Key
     */
    @GET("/data/2.5/onecall")
    suspend fun getFullForecast(
        @Query("lat") latitude: Latitude,
        @Query("lon") longitude: Longitude,
        @Query("lang") lang: String,
        @Query("units") units: String,
        @Query("appid") appId: String
    ): NetworkResponse<FullForecastResponseData, ErrorResponseData>

}