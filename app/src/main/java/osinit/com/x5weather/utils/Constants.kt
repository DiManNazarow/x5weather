package osinit.com.x5weather.utils

typealias Latitude = Double

typealias Longitude = Double

const val APP_SHARED_PREF_NAME = "X5Weather_sp"

const val CITIES_JSON_FILE_NAME = "city_list.json"

const val MOSCOW_CITY_ID = 524894L

const val MINSK_CITY_ID = 625144L