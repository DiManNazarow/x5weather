package osinit.com.x5weather.utils

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.Locale

fun Double.toTemperatureString(): String {
    val format = DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.getDefault()))
    format.maximumFractionDigits = 1
    return format.format(this)
}

fun Double.toWindSpeedString(): String {
    val format = DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.getDefault()))
    format.maximumFractionDigits = 2
    return format.format(this)
}

fun Double.toPopString(): String {
    val format = DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.getDefault()))
    format.maximumFractionDigits = 1
    return format.format(this)
}

fun View.hideKeyboard() {
    val imm: InputMethodManager =
        context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}

fun View.showKeyboard() {
    val imm: InputMethodManager =
        context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
}

inline fun <reified T> getTypeToken(): Type = object : TypeToken<T>(){}.type