package osinit.com.x5weather.repositories

import osinit.com.x5weather.models.Weather
import osinit.com.x5weather.networking.models.ErrorResponseData
import osinit.com.x5weather.networking.models.FullForecastResponseData
import osinit.com.x5weather.networking.models.NetworkResponse
import osinit.com.x5weather.utils.Latitude
import osinit.com.x5weather.utils.Longitude

/**
 * Repository to access weather data through API or DB
 */
interface WeatherRepository {

    /**
     * Request weather data from API
     */
    suspend fun request(latitude: Latitude, longitude: Longitude): NetworkResponse<FullForecastResponseData, ErrorResponseData>

    /**
     * Saves weather data to DB
     */
    suspend fun save(cityId: Long, responseData: FullForecastResponseData)

    /**
     * Gets weather data from DB to specific city
     * @param cityId - [osinit.com.x5weather.db.models.CityEntity.id]
     */
    suspend fun getForCity(cityId: Long): Weather?

}