package osinit.com.x5weather.repositories.impl

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import osinit.com.x5weather.BuildConfig
import osinit.com.x5weather.db.AppDatabase
import osinit.com.x5weather.mapping.WeatherMapping
import osinit.com.x5weather.models.Weather
import osinit.com.x5weather.networking.Api
import osinit.com.x5weather.networking.models.ErrorResponseData
import osinit.com.x5weather.networking.models.FullForecastResponseData
import osinit.com.x5weather.networking.models.NetworkResponse
import osinit.com.x5weather.repositories.WeatherRepository
import osinit.com.x5weather.utils.Latitude
import osinit.com.x5weather.utils.Longitude
import java.util.Locale

/**
 * Impl of [WeatherRepository]
 */
class WeatherRepositoryImpl(
    private val api: Api,
    private val appDatabase: AppDatabase
) : WeatherRepository {

    override suspend fun request(latitude: Latitude, longitude: Longitude): NetworkResponse<FullForecastResponseData, ErrorResponseData> = api.getFullForecast(
        latitude = latitude,
        longitude = longitude,
        lang = Locale.getDefault().language,
        units = MeasurementUnits.METRIC.rawValue,
        appId = BuildConfig.API_KEY
    )

    override suspend fun save(cityId: Long, responseData: FullForecastResponseData) = withContext(Dispatchers.IO) {
        val currentWeather = WeatherMapping.fullForecastResponseToCurrentWeatherEntity(cityId, responseData)
        val hourlyWeather = WeatherMapping.fullForecastResponseToHourlyWeatherEntities(cityId, responseData)
        val dailyWeather = WeatherMapping.fullForecastResponseToDailyWeatherEntities(cityId, responseData)
        appDatabase.weatherDao().deleteCurrentWeatherForCity(cityId)
        appDatabase.weatherDao().deleteHourlyWeatherForCity(cityId)
        appDatabase.weatherDao().deleteDailyWeatherForCity(cityId)
        appDatabase.weatherDao().insertCurrentWeather(currentWeather)
        appDatabase.weatherDao().insertHourlyWeather(hourlyWeather)
        appDatabase.weatherDao().insertDailyWeather(dailyWeather)
    }

    override suspend fun getForCity(cityId: Long): Weather? = withContext(Dispatchers.IO) {
        val weather = appDatabase.weatherDao().getWeatherForCity(cityId) ?: return@withContext null
        return@withContext WeatherMapping.weatherEntityToWeather(weather)
    }

}

enum class MeasurementUnits(val rawValue: String) {
    METRIC(rawValue = "metric")
}