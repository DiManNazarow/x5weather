package osinit.com.x5weather.repositories.impl

import android.content.Context
import android.content.SharedPreferences
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import osinit.com.x5weather.repositories.SettingsRepository
import osinit.com.x5weather.utils.APP_SHARED_PREF_NAME

/**
 * Impl of [SettingsRepository]
 */
class SettingsRepositoryImpl(
    context: Context
) : SettingsRepository {

    private val appSharedPreferences: SharedPreferences =
        context.getSharedPreferences(APP_SHARED_PREF_NAME, Context.MODE_PRIVATE)

    override suspend fun getCityId(): Long  = withContext(Dispatchers.IO) {
        return@withContext appSharedPreferences.getLong(SharedPrefKeys.CITY_ID.rawValue, -1)
    }

    override suspend fun saveCityId(id: Long) = withContext(Dispatchers.IO) {
        appSharedPreferences.edit().putLong(SharedPrefKeys.CITY_ID.rawValue, id).apply()
        return@withContext
    }

}

enum class SharedPrefKeys(val rawValue: String) {
    CITY_ID(rawValue = "city_id")
}