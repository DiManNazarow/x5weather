package osinit.com.x5weather.repositories

/**
 * Repository to access app settings and operational data
 */
interface SettingsRepository {

    /**
     * Gets selected city id
     * @return [osinit.com.x5weather.db.models.CityEntity.id]
     */
    suspend fun getCityId(): Long

    /**
     * Saved selected city id
     * @param id - [osinit.com.x5weather.db.models.CityEntity.id]
     */
    suspend fun saveCityId(id: Long)

}