package osinit.com.x5weather.repositories

import osinit.com.x5weather.models.City

/**
 * Repository to access cities
 */
interface CitiesRepository {

    /**
     * Returns cities marked as "saved" from DB
     */
    suspend fun getSaved(): List<City>

    /**
     * Returns specific city
     * @param cityId - [osinit.com.x5weather.db.models.CityEntity.id]
     */
    suspend fun get(cityId: Long): City?

    /**
     * Searches among cities the ones with given name
     * @param searchText - name of the cities to search for
     */
    suspend fun search(searchText: String?): List<City>

    /**
     * Marks city as "saved"
     * @param cityId - [osinit.com.x5weather.db.models.CityEntity.id]
     */
    suspend fun save(cityId: Long)

    /**
     * Remove "saved" mark
     * @param cityId - [osinit.com.x5weather.db.models.CityEntity.id]
     */
    suspend fun removeFromSaved(cityId: Long)

}