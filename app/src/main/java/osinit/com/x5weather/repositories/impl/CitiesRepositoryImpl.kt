package osinit.com.x5weather.repositories.impl

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import osinit.com.x5weather.db.AppDatabase
import osinit.com.x5weather.mapping.CitiesMapping
import osinit.com.x5weather.models.City
import osinit.com.x5weather.repositories.CitiesRepository

/**
 * Impl of [CitiesRepository]
 */
class CitiesRepositoryImpl(
    private val appDatabase: AppDatabase
) : CitiesRepository {

    override suspend fun getSaved(): List<City> = withContext(Dispatchers.IO) {
        val cities = appDatabase.citiesDao().getSaved()
        return@withContext CitiesMapping.cityEntitiesToCities(cities)
    }

    override suspend fun get(cityId: Long): City? = withContext(Dispatchers.IO) {
        val city = appDatabase.citiesDao().get(cityId) ?: return@withContext null
        return@withContext CitiesMapping.cityEntityToCity(city)
    }

    override suspend fun search(searchText: String?): List<City> = withContext(Dispatchers.IO) {
        if (searchText != null) {
            val cities = appDatabase.citiesDao().search("$searchText%")
            return@withContext CitiesMapping.cityEntitiesToCities(cities)
        } else {
            return@withContext emptyList()
        }
    }

    override suspend fun save(cityId: Long) = withContext(Dispatchers.IO) {
        appDatabase.citiesDao().save(cityId)
    }

    override suspend fun removeFromSaved(cityId: Long) = withContext(Dispatchers.IO) {
        appDatabase.citiesDao().removeFromSaved(cityId)
    }

}