package osinit.com.x5weather.usecases.city

import osinit.com.x5weather.repositories.SettingsRepository

/**
 * Use case to save selected city id
 */
class SaveCityIdUseCase(
    private val settingsRepository: SettingsRepository
) {

    suspend fun saveCityId(id: Long) = settingsRepository.saveCityId(id)

}