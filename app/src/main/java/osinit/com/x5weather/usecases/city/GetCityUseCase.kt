package osinit.com.x5weather.usecases.city

import osinit.com.x5weather.models.City
import osinit.com.x5weather.repositories.CitiesRepository

/**
 * Use case to get specific city
 */
class GetCityUseCase(
    private val citiesRepository: CitiesRepository
) {

    suspend fun getCityById(id: Long): City? = citiesRepository.get(cityId = id)

}