package osinit.com.x5weather.usecases.city

import osinit.com.x5weather.repositories.CitiesRepository

/**
 * Use case to search cities
 */
class SearchCitiesUseCase(
    private val citiesRepository: CitiesRepository
) {

    suspend fun search(searchClause: String?) = citiesRepository.search(searchClause)

}