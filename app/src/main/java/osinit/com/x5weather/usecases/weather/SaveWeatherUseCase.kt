package osinit.com.x5weather.usecases.weather

import osinit.com.x5weather.networking.models.FullForecastResponseData
import osinit.com.x5weather.repositories.SettingsRepository
import osinit.com.x5weather.repositories.WeatherRepository

/**
 * Use case to save city from API to DB
 */
class SaveWeatherUseCase(
    private val weatherRepository: WeatherRepository,
    private val settingsRepository: SettingsRepository
) {

    suspend fun save(responseData: FullForecastResponseData) {
        weatherRepository.save(settingsRepository.getCityId(), responseData)
    }

}