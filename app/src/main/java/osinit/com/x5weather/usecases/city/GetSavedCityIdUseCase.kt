package osinit.com.x5weather.usecases.city

import osinit.com.x5weather.repositories.SettingsRepository

/**
 * Use case to get id of currently selected city
 */
class GetSavedCityIdUseCase(
    private val settingsRepository: SettingsRepository
) {

    suspend fun getSavedCityId(): Long = settingsRepository.getCityId()

}