package osinit.com.x5weather.usecases.city

import osinit.com.x5weather.repositories.CitiesRepository

/**
 * Use case to mark city "saved"
 */
class SaveCityUseCase(
    private val citiesRepository: CitiesRepository
) {

    suspend fun save(id: Long) = citiesRepository.save(id)

}