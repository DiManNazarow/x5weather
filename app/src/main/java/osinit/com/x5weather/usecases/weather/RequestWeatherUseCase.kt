package osinit.com.x5weather.usecases.weather

import osinit.com.x5weather.networking.models.ErrorResponseData
import osinit.com.x5weather.networking.models.FullForecastResponseData
import osinit.com.x5weather.networking.models.NetworkResponse
import osinit.com.x5weather.repositories.WeatherRepository
import osinit.com.x5weather.utils.Latitude
import osinit.com.x5weather.utils.Longitude

/**
 * Use case to load weather from API
 */
class RequestWeatherUseCase(
    private val weatherRepository: WeatherRepository
) {

    suspend fun request(latitude: Latitude, longitude: Longitude): NetworkResponse<FullForecastResponseData, ErrorResponseData> = weatherRepository.request(latitude, longitude)

}