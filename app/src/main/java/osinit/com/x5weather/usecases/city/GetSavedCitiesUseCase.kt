package osinit.com.x5weather.usecases.city

import osinit.com.x5weather.repositories.CitiesRepository

/**
 * Use case to get cities marked "saved"
 */
class GetSavedCitiesUseCase(
    private val citiesRepository: CitiesRepository
) {

    suspend fun get() = citiesRepository.getSaved()

}