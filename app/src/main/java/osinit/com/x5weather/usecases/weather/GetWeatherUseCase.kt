package osinit.com.x5weather.usecases.weather

import osinit.com.x5weather.models.Weather
import osinit.com.x5weather.repositories.SettingsRepository
import osinit.com.x5weather.repositories.WeatherRepository

/**
 * Use case to load weather from DB
 */
class GetWeatherUseCase(
    private val weatherRepository: WeatherRepository,
    private val settingsRepository: SettingsRepository
) {

    suspend fun getWeather(): Weather? = weatherRepository.getForCity(settingsRepository.getCityId())

}