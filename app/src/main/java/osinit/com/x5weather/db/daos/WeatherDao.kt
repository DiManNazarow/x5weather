package osinit.com.x5weather.db.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import osinit.com.x5weather.db.models.CurrentWeatherEntity
import osinit.com.x5weather.db.models.DailyWeatherEntity
import osinit.com.x5weather.db.models.HourlyWeatherEntity
import osinit.com.x5weather.db.models.WeatherEntity

@Dao
interface WeatherDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCurrentWeather(currentWeatherEntity: CurrentWeatherEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDailyWeather(dailyWeather: List<DailyWeatherEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertHourlyWeather(hourlyWeather: List<HourlyWeatherEntity>)

    @Transaction
    @Query("SELECT * FROM CurrentWeather WHERE _id = :cityId")
    fun getWeatherForCity(cityId: Long): WeatherEntity?

    @Query("DELETE FROM CurrentWeather WHERE _id = :cityId")
    fun deleteCurrentWeatherForCity(cityId: Long)

    @Query("DELETE FROM HourlyWeather WHERE city_id = :cityId")
    fun deleteHourlyWeatherForCity(cityId: Long)

    @Query("DELETE FROM DailyWeather WHERE city_id = :cityId")
    fun deleteDailyWeatherForCity(cityId: Long)

}