package osinit.com.x5weather.db.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Entity to current weather condition
 */
@Entity(tableName = "CurrentWeather")
data class CurrentWeatherEntity(
    @PrimaryKey
    @ColumnInfo(name = "_id")
    val id: Long,
    @ColumnInfo(name = "current_temp")
    val currentTemp: Double,
    /**
     * Text description of weather like "windy" of "cloudy"
     */
    @ColumnInfo(name = "current_condition")
    val currentCondition: String?,
    /**
     * Actual feeling of temp
     */
    @ColumnInfo(name = "feels_like")
    val feelsLike: Double,
    /**
     * Icon name to use in link generator (not actual link)
     */
    @ColumnInfo(name = "weather_icon")
    val weatherIcon: String?
)