package osinit.com.x5weather.db.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import osinit.com.x5weather.db.models.CityEntity

/**
 * Dao to access cities in DB
 */
@Dao
interface CitiesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(cities: List<CityEntity>)

    @Query("SELECT * FROM City WHERE name LIKE :searchClause COLLATE NOCASE")
    fun search(searchClause: String): List<CityEntity>

    @Query("SELECT * FROM City")
    fun getAll(): List<CityEntity>

    /**
     * Gets all cities with "saved" mark
     */
    @Query("SELECT * FROM City WHERE saved = 1")
    fun getSaved(): List<CityEntity>

    @Query("SELECT * FROM City WHERE _id = :id")
    fun get(id: Long): CityEntity?

    @Query("SELECT count() FROM City")
    fun count(): Int

    /**
     * Marks city as saved after selection on list
     */
    @Query("UPDATE City SET saved = 1 WHERE _id = :id")
    fun save(id: Long)

    /**
     * Removes "saved" mark
     */
    @Query("UPDATE City SET saved = 0 WHERE _id = :id")
    fun removeFromSaved(id: Long)

}