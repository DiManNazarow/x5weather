package osinit.com.x5weather.db.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import osinit.com.x5weather.utils.Latitude
import osinit.com.x5weather.utils.Longitude

/**
 * Entity to cities
 */
@Entity(tableName = "City")
class CityEntity(
    @PrimaryKey
    @ColumnInfo(name = "_id")
    val id: Long,
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "country")
    val country: String,
    @ColumnInfo(name = "latitude")
    val latitude: Latitude,
    @ColumnInfo(name = "longitude")
    val longitude: Longitude,
    /**
     * Does city has appeared in search and was chosen?
     * True if was
     * False if not
     */
    @ColumnInfo(name = "saved")
    val saved: Boolean
)