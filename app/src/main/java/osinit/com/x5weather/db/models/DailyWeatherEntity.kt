package osinit.com.x5weather.db.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Entity of daily weather
 */
@Entity(tableName = "DailyWeather")
data class DailyWeatherEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "_id")
    val id: Long,
    @ColumnInfo(name = "city_id")
    val cityId: Long,
    @ColumnInfo(name = "time")
    val time: Long,
    @ColumnInfo(name = "day_temp")
    val dayTemp: Double,
    @ColumnInfo(name = "day_temp_feels_like")
    val dayTempFeelsLike: Double,
    @ColumnInfo(name = "night_temp")
    val nightTemp: Double,
    @ColumnInfo(name = "night_temp_feels_like")
    val nightTempFeelsLike: Double,
    @ColumnInfo(name = "humidity")
    val humidity: Int,
    /**
     * Icon name to use in link generator (not actual link)
     */
    @ColumnInfo(name = "weather_icon")
    val weatherIcon: String?,
    @ColumnInfo(name = "wind_degree")
    val windDegree: Int,
    @ColumnInfo(name = "wind_speed")
    val windSpeed: Double,
    /**
     * Probability of precipitation
     */
    @ColumnInfo(name = "pop")
    val pop: Double
)