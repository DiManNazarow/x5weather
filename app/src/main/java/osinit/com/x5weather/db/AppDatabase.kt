package osinit.com.x5weather.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import osinit.com.x5weather.BuildConfig
import osinit.com.x5weather.db.daos.CitiesDao
import osinit.com.x5weather.db.daos.WeatherDao
import osinit.com.x5weather.db.models.CityEntity
import osinit.com.x5weather.db.models.CurrentWeatherEntity
import osinit.com.x5weather.db.models.DailyWeatherEntity
import osinit.com.x5weather.db.models.HourlyWeatherEntity

@Database(version = 2, exportSchema = true, entities = [
    CurrentWeatherEntity::class,
    DailyWeatherEntity::class,
    HourlyWeatherEntity::class,
    CityEntity::class
])
abstract class AppDatabase : RoomDatabase() {

    abstract fun citiesDao(): CitiesDao

    abstract fun weatherDao(): WeatherDao

    companion object {

        /**
         * Build the instance of database
         * @param context - [Context] prefer to be application context
         */
        fun buildDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, "X5Weather_DB.db")
                .createFromAsset("X5Weather_DB.db")
                .build()
        }

    }

}