package osinit.com.x5weather.db.models

import androidx.room.Embedded
import androidx.room.Relation

data class WeatherEntity(
    @Embedded
    val currentWeather: CurrentWeatherEntity,
    @Relation(parentColumn = "_id", entityColumn = "city_id")
    val hourlyWeather: List<HourlyWeatherEntity>,
    @Relation(parentColumn = "_id", entityColumn = "city_id")
    val dailyWeather: List<DailyWeatherEntity>
)