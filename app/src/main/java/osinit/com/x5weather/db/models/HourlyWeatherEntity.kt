package osinit.com.x5weather.db.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Entity to hourly weather
 */
@Entity(tableName = "HourlyWeather")
data class HourlyWeatherEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "_id")
    val id: Long,
    @ColumnInfo(name = "city_id")
    val cityId: Long,
    @ColumnInfo(name = "time")
    val time: Long,
    @ColumnInfo(name = "temp")
    val temp: Double,
    @ColumnInfo(name = "feels_like")
    val feelsLike: Double,
    @ColumnInfo(name = "humidity")
    val humidity: Int,
    @ColumnInfo(name = "current_condition")
    val currentCondition: String?,
    /**
     * Icon name to use in link generator (not actual link)
     */
    @ColumnInfo(name = "weather_icon")
    val weatherIcon: String?,
    /**
     * Probability of precipitation
     */
    @ColumnInfo(name = "pop")
    val pop: Double,
    @ColumnInfo(name = "wind_speed")
    val windSpeed: Double,
    @ColumnInfo(name = "wind_degree")
    val windDegree: Int
)