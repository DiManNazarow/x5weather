package osinit.com.x5weather.di

import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import osinit.com.x5weather.db.AppDatabase
import osinit.com.x5weather.networking.ApiClient
import osinit.com.x5weather.repositories.CitiesRepository
import osinit.com.x5weather.repositories.SettingsRepository
import osinit.com.x5weather.repositories.WeatherRepository
import osinit.com.x5weather.repositories.impl.CitiesRepositoryImpl
import osinit.com.x5weather.repositories.impl.SettingsRepositoryImpl
import osinit.com.x5weather.repositories.impl.WeatherRepositoryImpl
import osinit.com.x5weather.ui.viewmodels.CitiesViewModel
import osinit.com.x5weather.ui.dailyforecast.DailyForecastFragmentViewModel
import osinit.com.x5weather.ui.hourlyforecast.HourlyForecastFragmentViewModel
import osinit.com.x5weather.ui.viewmodels.WeatherViewModel
import osinit.com.x5weather.usecases.city.SearchCitiesUseCase
import osinit.com.x5weather.usecases.city.GetCityUseCase
import osinit.com.x5weather.usecases.city.GetSavedCitiesUseCase
import osinit.com.x5weather.usecases.city.GetSavedCityIdUseCase
import osinit.com.x5weather.usecases.city.SaveCityIdUseCase
import osinit.com.x5weather.usecases.city.SaveCityUseCase
import osinit.com.x5weather.usecases.weather.GetWeatherUseCase
import osinit.com.x5weather.usecases.weather.RequestWeatherUseCase
import osinit.com.x5weather.usecases.weather.SaveWeatherUseCase

val appModule = module {
    single { ApiClient.create() }
    single { AppDatabase.buildDatabase(androidContext()) }
}

val repositoriesModule = module {
    single<WeatherRepository> { WeatherRepositoryImpl(get(), get()) }
    single<SettingsRepository> { SettingsRepositoryImpl(get()) }
    single<CitiesRepository> { CitiesRepositoryImpl(get()) }
}

val useCasesModule = module {
    factory { RequestWeatherUseCase(get()) }
    factory { SaveWeatherUseCase(get(), get()) }
    factory { GetWeatherUseCase(get(), get()) }
    factory { SearchCitiesUseCase(get()) }
    factory { GetCityUseCase(get()) }
    factory { GetSavedCityIdUseCase(get()) }
    factory { SaveCityIdUseCase(get()) }
    factory { GetSavedCitiesUseCase(get()) }
    factory { SaveCityUseCase(get()) }
}

val viewModelsModule = module {
    viewModel { CitiesViewModel(get(), get(), get(), get(), get(), get()) }
    viewModel { DailyForecastFragmentViewModel() }
    viewModel { HourlyForecastFragmentViewModel() }
    viewModel { WeatherViewModel(get(), get(), get(), get(), get()) }
}