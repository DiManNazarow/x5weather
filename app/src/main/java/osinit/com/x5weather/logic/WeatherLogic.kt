package osinit.com.x5weather.logic

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

object WeatherLogic {

    /**
     * Generates url to icon
     * @param iconName - name of icon
     */
    fun createWeatherIconUrl(iconName: String?): String? = iconName?.let {
        return "https://openweathermap.org/img/wn/${it}.png"
    }

    /**
     * Converts long timestamp to text date like "вс 25"
     */
    fun longTimeToDayWithDate(time: Long): String {
        val date = Date(time)
        val formatter = SimpleDateFormat("EEE dd", Locale.getDefault())
        return formatter.format(date)
    }

    /**
     * Converts long timestamp to text date like "23:00 пн"
     */
    fun longTimeToHourAndMinAndDay(time: Long): String {
        val date = Date(time)
        val formatter = SimpleDateFormat("HH:mm EEE", Locale.getDefault())
        return formatter.format(date)
    }

}