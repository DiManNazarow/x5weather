package osinit.com.x5weather

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import osinit.com.x5weather.di.appModule
import osinit.com.x5weather.di.repositoriesModule
import osinit.com.x5weather.di.useCasesModule
import osinit.com.x5weather.di.viewModelsModule

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(listOf(appModule, repositoriesModule, useCasesModule, viewModelsModule))
        }
    }

}