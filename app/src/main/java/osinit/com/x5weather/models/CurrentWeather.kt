package osinit.com.x5weather.models

data class CurrentWeather(
    /**
     * Current temperature
     */
    val currentTemp: String,
    /**
     * Text description of weather like "windy" of "cloudy"
     */
    val currentCondition: String,
    /**
     * Real feel of temperature
     */
    val feelsLike: String,
    /**
     * Icon name to use in link generator (not actual link)
     */
    val weatherIcon: String?
)