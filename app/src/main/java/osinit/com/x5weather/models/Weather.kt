package osinit.com.x5weather.models

/**
 * Contains current weather, hourly and daily
 */
data class Weather(
    val currentWeather: CurrentWeather,
    val dailyWeather: List<DayWeather>,
    val hourlyWeather: List<HourWeather>
)

val WEATHER_STUB = Weather(
    currentWeather = CurrentWeather(
        currentTemp = "0.0",
        currentCondition = "Ясно",
        feelsLike = "0.0",
        weatherIcon = null
    ),
    hourlyWeather = listOf(
        HourWeather(
            time = 1603464476000,
            temp = "0.0",
            feelsLike = "0.0",
            humidity = "30",
            description = "",
            iconLink = null,
            windDegree = "270",
            windSpeed = "14",
            pop = "0.5"
        ),
        HourWeather(
            time = 1603464476000,
            temp = "0.0",
            feelsLike = "0.0",
            humidity = "30",
            description = "",
            iconLink = null,
            windDegree = "270",
            windSpeed = "14",
            pop = "0.5"
        ),
        HourWeather(
            time = 1603464476000,
            temp = "0.0",
            feelsLike = "0.0",
            humidity = "30",
            description = "",
            iconLink = null,
            windDegree = "270",
            windSpeed = "14",
            pop = "0.5"
        ),
        HourWeather(
            time = 1603464476000,
            temp = "0.0",
            feelsLike = "0.0",
            humidity = "30",
            description = "",
            iconLink = null,
            windDegree = "270",
            windSpeed = "14",
            pop = "0.5"
        ),
        HourWeather(
            time = 1603464476000,
            temp = "0.0",
            feelsLike = "0.0",
            humidity = "30",
            description = "",
            iconLink = null,
            windDegree = "270",
            windSpeed = "14",
            pop = "0.5"
        ),
        HourWeather(
            time = 1603464476000,
            temp = "0.0",
            feelsLike = "0.0",
            humidity = "30",
            description = "",
            iconLink = null,
            windDegree = "270",
            windSpeed = "14",
            pop = "0.5"
        ),
        HourWeather(
            time = 1603464476000,
            temp = "0.0",
            feelsLike = "0.0",
            humidity = "30",
            description = "",
            iconLink = null,
            windDegree = "270",
            windSpeed = "14",
            pop = "0.5"
        )
    ),
    dailyWeather = listOf(
        DayWeather(
            time = "пн 21",
            dayTemp = "0.0",
            dayTempFeelsLike = "0.0",
            nightTemp = "0.0",
            nightTempFeelsLike = "0.0",
            humidity = "30",
            iconLink = null,
            windSpeed = "10",
            windDegree = "90",
            pop = "20"
        ),
        DayWeather(
            time = "вт 22",
            dayTemp = "0.0",
            dayTempFeelsLike = "0.0",
            nightTemp = "0.0",
            nightTempFeelsLike = "0.0",
            humidity = "30",
            iconLink = null,
            windSpeed = "10",
            windDegree = "90",
            pop = "20"
        ),
        DayWeather(
            time = "ср 23",
            dayTemp = "0.0",
            dayTempFeelsLike = "0.0",
            nightTemp = "0.0",
            nightTempFeelsLike = "0.0",
            humidity = "30",
            iconLink = null,
            windSpeed = "10",
            windDegree = "90",
            pop = "20"
        ),
        DayWeather(
            time = "чт 24",
            dayTemp = "0.0",
            dayTempFeelsLike = "0.0",
            nightTemp = "0.0",
            nightTempFeelsLike = "0.0",
            humidity = "30",
            iconLink = null,
            windSpeed = "10",
            windDegree = "90",
            pop = "20"
        ),
        DayWeather(
            time = "пт 25",
            dayTemp = "0.0",
            dayTempFeelsLike = "0.0",
            nightTemp = "0.0",
            nightTempFeelsLike = "0.0",
            humidity = "30",
            iconLink = null,
            windSpeed = "10",
            windDegree = "90",
            pop = "20"
        ),
        DayWeather(
            time = "сб 26",
            dayTemp = "0.0",
            dayTempFeelsLike = "0.0",
            nightTemp = "0.0",
            nightTempFeelsLike = "0.0",
            humidity = "30",
            iconLink = null,
            windSpeed = "10",
            windDegree = "90",
            pop = "20"
        ),
        DayWeather(
            time = "вс 27",
            dayTemp = "0.0",
            dayTempFeelsLike = "0.0",
            nightTemp = "0.0",
            nightTempFeelsLike = "0.0",
            humidity = "30",
            iconLink = null,
            windSpeed = "10",
            windDegree = "90",
            pop = "20"
        )
    )
)