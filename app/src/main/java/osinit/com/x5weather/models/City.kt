package osinit.com.x5weather.models

import osinit.com.x5weather.utils.Latitude
import osinit.com.x5weather.utils.Longitude

data class City(
    /**
     * Id of city
     */
    val id: Long,
    /**
     * Name of city (en)
     */
    val name: String,
    /**
     * Country code of city (eg. RU)
     */
    val county: String,

    val latitude: Latitude,

    val longitude: Longitude,
    /**
     * Saved mark
     */
    val saved: Boolean

)