package osinit.com.x5weather.models

data class HourWeather(
    /**
     * Time of forecast (long timestamp ms)
     */
    val time: Long,
    /**
     * Temperature
     */
    val temp: String,
    /**
     * Real feel of temperature
     */
    val feelsLike: String,
    /**
     * Percent of humidity
     */
    val humidity: String,
    /**
     * Text description of weather like "windy" of "cloudy"
     */
    val description: String,
    /**
     * Icon name to use in link generator (not actual link)
     */
    val iconLink: String?,
    /**
     * Wind direction
     */
    val windDegree: String,
    /**
     * Wind speed in meters per seconds
     */
    val windSpeed: String,
    /**
     * Probability of precipitation
     */
    val pop: String
)