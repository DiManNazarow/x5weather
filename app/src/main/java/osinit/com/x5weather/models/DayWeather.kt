package osinit.com.x5weather.models

data class DayWeather(
    /**
     * Time of forecast (long timestamp ms)
     */
    val time: String,
    /**
     * Temp of day
     */
    val dayTemp: String,
    /**
     * Real feel of day temp
     */
    val dayTempFeelsLike: String,
    /**
     * Night temp
     */
    val nightTemp: String,
    /**
     * Real feel of night temp
     */
    val nightTempFeelsLike: String,
    /**
     * Percent of humidity
     */
    val humidity: String,
    /**
     * Icon name to use in link generator (not actual link)
     */
    val iconLink: String?,
    /**
     * Wind direction
     */
    val windDegree: String,
    /**
     * Wind speed in meters per seconds
     */
    val windSpeed: String,
    /**
     * Probability of precipitation
     */
    val pop: String
)